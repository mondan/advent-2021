package main

import (
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/mondan/advent-2021/day1"
)

func main() {
	input, err := readInput(os.Args[1])
	if err != nil {
		panic(err)
	}

	part1 := day1.Part1(input)
	part2 := day1.Part2(input)

	log.Printf("Part1: %d\n", part1)
	log.Printf("Part2: %d\n", part2)
}

func readInput(path string) ([]int, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(data), "\n")
	
	m := make([]int, len(lines))
	for i, l := range lines {
		m[i], _ = strconv.Atoi(string(l))
	}

	return m, nil
}
