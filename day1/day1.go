package day1

func Part1(measurements []int) int {
	count := 0
	last := measurements[0]

	for _, i := range measurements[1:] {
		if i > last {
			count += 1
		}

		last = i
	}

	return count
}

func Part2(measurements []int) int {
	length := len(measurements)
	count := 0
	last := measurements[0] + measurements[1] + measurements[2]

	for i := 1; i < length-2; i++ {
		next := measurements[i] + measurements[i+1] + measurements[i+2]
		if next > last {
			count++
		}

		last = next
	}
	
	return count
}
