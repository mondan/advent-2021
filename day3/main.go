package main

import (
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	input, err := readInput(os.Args[1])
	if err != nil {
		panic(err)
	}

	part1 := part1(input)
	part2 := part2(input)

	log.Printf("part1: %d\n", part1)
	log.Printf("part2: %d\n", part2)
}

func readInput(path string) ([]string, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(data), "\n")

	return lines, nil
}

func part1(input []string) int64 {
	gamma := ""
	epsilon := ""

	inputLength := len(input)
	lineLength := len(input[0])

	halfInputLength := inputLength / 2

	for i := 0; i < lineLength; i++ {
		r := sumInt(mapInt(input, i))

		if r > halfInputLength {
			gamma += "1"
			epsilon += "0"
			continue
		}

		gamma += "0"
		epsilon += "1"
	}

	gammaInt, _ := strconv.ParseInt(gamma, 2, 16)
	epsilonInt, _ := strconv.ParseInt(epsilon, 2, 16)

	return gammaInt * epsilonInt
}

func part2(input []string) int64 {
	o := input
	c := input
	lineLength := len(input[0])

	for i := 0; i < lineLength; i++ {
		if len(o) > 1 {
			o = filterInput(o, i, oxyBit)
		}

		if len(c) > 1 {
			c = filterInput(c, i, co2Bit)
		}
	}

	ov, _ := strconv.ParseInt(o[0], 2, 16)
	cv, _ := strconv.ParseInt(c[0], 2, 16)

	return ov * cv
}

func mapInt(a []string, idx int) []int {
	r := make([]int, len(a))
	for i, v := range a {
		r[i], _ = strconv.Atoi(string(v[idx]))
	}
	return r
}

func sumInt(a []int) int {
	r := 0
	for _, i := range a {
		r += i
	}
	return r
}

func filterInput(a []string, idx int, sv func(float64, float64) uint8) []string {
	r := sumInt(mapInt(a, idx))
	i := []string{}

	h := float64(len(a)) / 2.0
	v := sv(float64(r), h)

	for _, l := range a {
		if l[idx] == v {
			i = append(i, l)
		}
	}

	return i
}

func oxyBit(r, h float64) uint8 {
	if r > h || r == h {
		return '1'
	}
	return '0'
}

func co2Bit(r, h float64) uint8 {
	if r > h || r == h {
		return '0'
	}
	return '1'
}
