package day2

type Instruction struct {
	Direction string
	Value     int
}

func Part1(instructions []Instruction) int {
	horz := 0
	depth := 0

	for _, i := range instructions {
		switch i.Direction {
		case "forward":
			horz += i.Value
		case "up":
			depth -= i.Value
		case "down":
			depth += i.Value
		}
	}

	return horz * depth
}

func Part2(instructions []Instruction) int {
	horz := 0
	depth := 0
	aim := 0

	for _, i := range instructions {
		switch i.Direction {
		case "forward":
			horz += i.Value
			depth += aim * i.Value
		case "up":
			aim -= i.Value
		case "down":
			aim += i.Value
		}
	}

	return horz * depth
}
