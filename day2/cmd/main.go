package main

import (
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/mondan/advent-2021/day2"
)

func main() {
	input, err := readInput(os.Args[1])
	if err != nil {
		panic(err)
	}

	part1 := day2.Part1(input)
	part2 := day2.Part2(input)

	log.Printf("Part1 %d\n", part1)
	log.Printf("Part2 %d\n", part2)
}

func readInput(path string) ([]day2.Instruction, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(data), "\n")

	m := make([]day2.Instruction, len(lines))
	for i, l := range lines {
		p := strings.Split(string(l), " ")

		m[i] = day2.Instruction{
			Direction: p[0],
		}

		m[i].Value, _ = strconv.Atoi(string(p[1]))
	}

	return m, nil
}
